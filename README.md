# GoldenEye: Source Documentation Website

This is the repository for the GoldenEye: Source documentation website. It uses Hugo to generate static pages that are then synced to a web hosting service.

## How the website is updated

The documentation is in the `[ges-docs](https://gitlab.com/gesource/ges-docs-website)` repository and is included as a submodule here in the `content` folder.

When someone pushes to the `ges-docs` repository, those changes are automatically built and pushed to https://docs.geshl2.com.
