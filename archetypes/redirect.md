---
date: {{ .Date }}
type: "redirect"
redirect: "https://..."
---

Content not rendered. You can use this space for notes etc.
